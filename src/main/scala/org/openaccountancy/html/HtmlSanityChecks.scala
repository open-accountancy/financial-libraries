package org.openaccountancy.html;

/*

Copyright 2023 - Jeffrey Paul Prickett 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/**
 * The purpose of this source file is to define regular expressions used in 
 * assertions to screen out malicious or invalid inputs from being included in
 * our html controls.
 */

import scala.util.matching.Regex; 

object HtmlSanityChecks {
    /**
     * This regular expression attempts to screen out invalid html/xml attribute
     * values.
     */
    private val xmlAttrValRegexp : Regex = """^[^<>"'`]*$""".r  
        
    /**
     * This regular expression attempts to screen out invalid xml/html
     * attribute names.
     *
     */
    private val xmlAttrNameRegexp : Regex = """^[a-zA-Z_][a-zA-Z0-9_-]*$""".r 
        

    /**
     * This function does a sanity check on the attribute value to make sure
     * it is a valid html/xml attribute value.
     * @param attrVal The attribute value to check for validity. 
     * @return Returns true of the attribute value is valid html, false if it
     *         is not valid html.      
     */
    def isValidHtmlAttrVal(attrVal : String) : Boolean =  {
        if(attrVal != null) {
            val trimmedVal : String = attrVal.trim();
            if(!trimmedVal.isEmpty) {
                return xmlAttrValRegexp.findFirstMatchIn(trimmedVal).isDefined
            } else {
                return false;
            }
        }  else {
            return false
        }
    }

    /**
     * This function does a sanity check on the attribute name to make sure it
     * is a valid html/xml attribute name.
     * @param attrName The attribute name to check validity for.
     * @return Returns true if the attribute name is valid html, false if it is
     *         not valid html.
     */
    def isValidHtmlAttrName(attrName : String) : Boolean = {
        if(attrName != null)
        {
            val trimmedName : String = attrName.trim();
            if(!trimmedName.isEmpty) {
                return xmlAttrNameRegexp.findFirstMatchIn(trimmedName).isDefined
            } else {
                return false
            }    
        } else {
            return false
        }
    }
}    
        
        
