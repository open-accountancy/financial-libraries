package org.openaccountancy.nato;

/*

Copyright 2023 - Jeffrey Paul Prickett 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/** Scala language imports. */
import scala.collection.mutable.ArrayBuffer; 

/**
 * The purpose of this source file is to define the NATO Phonetic Alphabet and
 * functions related to the phonetic alphabet.
 */
object NatoPhoneticAlphabet {
    
    /** This value contains the nato phonetic alphabet */
    val phoneticAlphabet : Array[String] = Array("Alpha", "Bravo", 
                "Charlie",
                "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliet",
                "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec",
                "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey",
                "Xray", "Yankee", "Zulu")
     
     /**
      * The purpose of this method is to return the phonetic alphabet in order
      * in all lowercase.
      * @return An array of strings containing the phonetic alphbet in 
      *         lowercase.
      */
     def lowercasePhoneticAlphabet() : Array[String]  = {
         val workingValue : ArrayBuffer[String] = new ArrayBuffer[String]();
         for(letter <- phoneticAlphabet) {
             workingValue += letter.toLowerCase
         }
         workingValue.toArray
     }

         
     /**
      * The purpose of this method is to return the phonetic alphabet in order
      * in all uppercase.
      * @return An array of strings containing the phonetic alphbet in 
      *         uppercase.
      */
     def uppercasePhoneticAlphabet() : Array[String]  = {
         val workingValue : ArrayBuffer[String] = new ArrayBuffer[String]();
         for(letter <- phoneticAlphabet) {
             workingValue += letter.toUpperCase
         }
         workingValue.toArray
     }
 }    
