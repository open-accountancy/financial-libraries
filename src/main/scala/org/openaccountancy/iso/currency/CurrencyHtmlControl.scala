package org.openaccountancy.iso.currency;

/* Java classes */
import java.util.Currency;

/* Scala language files */
import scala.collection.mutable.Map;

/* Open Accountancy Dependencies */
import org.openaccountancy.html.HtmlSanityChecks;

/**
 * The purpose of this source file is to define an HTML Currency Controls that 
 * includes a list of currencies you can specify. Plus you are able to specify
 * a list of currencies to prioritize so that those currencies appear in the
 * order you specify at the top of your currency control.
 */

/**
 * The purpose of this class is to create a currency control that 
 * contains all the currencies available to the JVM except 
 * the currencies you specify that you want to exclude. The currencies 
 * that you prioritize will appear first in the control in the order
 * that you list them.
 */
class CurrencyHtmlTextControl(textboxIdin : String,  
         datalistId: String, 
         uiLabel : String, labelAttrs : Map[String,String] , 
         textboxAttrs : Map[String,String], 
         datalistAttrs : Map[String,String], 
         listItemAttrs : Map[String,String], 
         currencyLists : Set[Currency]*) {
    val textboxId : String = textboxIdin
}    
    
            
                            
            
        

     

