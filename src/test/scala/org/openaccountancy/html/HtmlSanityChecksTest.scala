package org.openaccountancy.html;

/*

Copyright 2023 - Jeffrey Paul Prickett 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/**
 * The purpose of this source file is to test the Html Sanity Checks functions.
 */

/* Scala Test Class Imports */
import org.scalatest.funsuite.AnyFunSuite

/** Open Accountancy Imports */
import org.openaccountancy.html.HtmlSanityChecks;
import org.openaccountancy.nato.NatoPhoneticAlphabet;

class HtmlSanityChecksTest extends AnyFunSuite {
    
    val lowercaseAlpha : Array[String] = Array("a", "b", "c", "d", "e", "f", 
                "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", 
                "s", "t", "u", "v", "w", "x", "y", "z")

    val invalidPunctChars : Array[String] = Array("`", "~", "!", "@", "#", "$",
                "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
                "{", "}", "\\", "|", ";", ":", """'""", ",", ".", "?", "<", 
                ">", "/")

    /**
     * Test for null attribute value.
     */
    test("Null Attribute Value.") {
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(null));
    }

    /**
     * Test for an empty string attribute value.
     */
    test("Empty String Attribute Value")
    {
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(""));
    }    

    /**
     * Test for a valid attribute value.
     */
    test("Valid Attribute Value.") {
        val validAttribute : String = """abcdefghijklmnopqrstuvwxyz0123456789
            |01234567890-=!@#$%^&*()-=_+[]{}\|;:,./?"""
        info("validAttribute =\"" + validAttribute + "\"") 
        assert(HtmlSanityChecks.isValidHtmlAttrVal(validAttribute))
    }

    /**
     * Test for the illegal value "<"
     */
    test("Invalid Attribute Value \"<\"") {
        val invalidAttribute : String = "abcd<efgh"
        info("invalidAttributeVal=\"" + invalidAttribute + "\"");
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(invalidAttribute))
    }

    /**
     * Test for illegal value ">"
     */
    test("Invalid Attribute Value \">\"") {
        val invalidAttribute : String = ">abcdefgh"
        info("invalidAttributeVal=\"" + invalidAttribute + "\"")
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(invalidAttribute))
    }    

    /**
     * Test for illegal value " 
     */
    test("Invalid Attribute Value \"\"\"") {
        val invalidAttribute = "abcdefgh\""
        info("invalidAttributeVal=\"" + invalidAttribute + "\"") 
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(invalidAttribute))
    }    
   
    /**
     * Test for illegal value "'"
     */
    test("Invalid Attribute Value \"\'\"") {
        val invalidAttribute : String = """abcd'efgh"""
        info("""invalidAttributeVal=""""" + invalidAttribute + "\"")
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(invalidAttribute))
    }    

    /**
     * Test for illegal value "`"
     */
    test("Invalid Attribute Value \"`\"") {
        val invalidAttribute : String = """`"""
        info("""invalidAttributeVal="""" + invalidAttribute + """`""")
        assert(!HtmlSanityChecks.isValidHtmlAttrVal(invalidAttribute))
    }    

    /**
     * Test for null name.
     */
    test("Null Attribute Name") {
        assert(!HtmlSanityChecks.isValidHtmlAttrName(null))
    }

    /**
     * Test for empty string name.
     */
    test("Empty String Attribute Name.") {
        assert(!HtmlSanityChecks.isValidHtmlAttrName(""))
    }    

    /**
     * Test for attribute name that is all lower case letters 
     */
    test("Attribute Name that is all lower case letters") {
        for( currentName <- NatoPhoneticAlphabet.lowercasePhoneticAlphabet()) {
            info("currentName=\"" + currentName + "\"")
            assert(HtmlSanityChecks.isValidHtmlAttrName(currentName))
        } 
    }

    /**
     * Test for attribute name is all upper case letters
     */
    test("Attribute Name is all upper case letters") {
        for( currentName <- NatoPhoneticAlphabet.uppercasePhoneticAlphabet()) {
            info("currentName=\"" + currentName + "\"") 
            assert(HtmlSanityChecks.isValidHtmlAttrName(currentName))
        }
    }
    
    /**
     * Test for attribute name that starts with upper case letters
     */
    test("Attribute Name that starts with upper case letters") {
        for( currentName <- NatoPhoneticAlphabet.phoneticAlphabet) {
            info("currentName=\"" + currentName + "\"") 
            assert(HtmlSanityChecks.isValidHtmlAttrName(currentName))
        }
    }

    /**
     * Test for attribute name that are a single lower case alphabetic 
     * character.
     */
    test("Attribute name that is a single lowercase alphabetic character") {
        for( currentName <- lowercaseAlpha) {
            info("currentName\"" + currentName + "\"")
            assert(HtmlSanityChecks.isValidHtmlAttrName(currentName))
        }
    }    

    /**
     * Test for attribute name with numbers after the first letter.
     */
    test("Attribute name with numbers after the first letter.") {
        var num : Int = 0;
        for(currentName <- lowercaseAlpha ) {
            val currentVal = currentName + num
            info("currentValue=\"" + currentVal + "\"")
            assert(HtmlSanityChecks.isValidHtmlAttrName(currentVal))
            num += 1
        }
    }   

    /**
     * Test for attribute name that starts with an underscore.
     */
    test("Attribute name that starts with an underscore.") {
        val currentVal = "_alpha"
        info("currentValue=\"" + currentVal + "\"")
        assert(HtmlSanityChecks.isValidHtmlAttrName(currentVal))
    }    

    /**
     * Test for minus sign after the first character.
     */
    test("Attribute name contains minus sign after the first character.") {
        val attrName = "x-ray"
        info("currentValue=\"" + attrName +"\"")
        assert(HtmlSanityChecks.isValidHtmlAttrName(attrName))
    }    

    /**
     * Test for underscore after the first character.
     */
    test("Attribute name contains _ after the first character.") {
        val attName = "alpha_bravo"
        info("currentValue=\"" + attName + "\"");
        assert(HtmlSanityChecks.isValidHtmlAttrName(attName))
    }    


    /**
     * Test for attribute name that starts with numbers (illegal value)
     */
    test("Attribute name that starts with numbers (illegal value).") {
        val numbers : Array[String] = Array("0","1","2","3","4","5","6","7",
               "8","9","0a", "1b", "2c", "3d", "4e", "5f", "6g", "7h", "8i",
               "9j")
        for( number <- numbers) {
            info("number=\"" + number + "\"")
            assert(!HtmlSanityChecks.isValidHtmlAttrName(number))
        }
    }

    /**
     * Test for attribute names that start with punctuation characters
     */
    test("Attribute name that starts with punctuation (illegal value).") {
        for(punct <- invalidPunctChars ) {
            info("punctuation = \"" + punct + "\"")
            assert(!HtmlSanityChecks.isValidHtmlAttrName(punct))
        }
    }    

    /**
     * Test for attribute names that have punctuation after the first 
     * character
     */
    test("Attribute name that has punctuation after the first character.") {
        for(punct <- invalidPunctChars) {
            info("punctuation = \"a" + punct + "\"")
            assert(!HtmlSanityChecks.isValidHtmlAttrName("a" + punct))
        }
    }    
}
